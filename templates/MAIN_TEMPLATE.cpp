/*
MAIN_TEMPLATE.cpp
James Watson , YYYY MONTHNAME
A ONE-LINE DESCRIPTION OF THE FILE

Dependencies:
Template Version: 2018-06-06
*/

/*
	~~ DEV PLAN ~~
	[ ] TODO_i
*/

// === Init ================================================================================================================================

// ~~ Includes ~~


// ~ Special ~

// ~ Local ~ 
#include "Cpp_Helpers.h" // Shortcuts and Aliases , and Functions of general use for C++ programming

// ___ End Init ____________________________________________________________________________________________________________________________


// === Program Functions & Classes ===



// ___ End Functions & Classes ___


// === Program Vars ===



// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	sep( "I EXIST!" ); // Silly output test
	
	
	// DO STUFF HERE
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
